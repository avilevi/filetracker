Akka based file monitor
=========================

Asynchronously Monitors several directories using watch service and akka actors.
 
This project goal is to track changes in file and notify each change and it's position in the file . 
For complete description check [this wiki](https://bitbucket.org/avilevi/filetracker/wiki/File%20tracker) page

Usage :
Run modify the ApplicationMain class based on your directories and pattern