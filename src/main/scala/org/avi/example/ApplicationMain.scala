package org.avi.example

import akka.actor.ActorSystem
import akka.pattern.ask
import org.avi.monitor.CommonMessages.Init
import org.avi.monitor._
import akka.util.Timeout
import scala.concurrent.duration._
import scala.collection.mutable
import scala.util.{Failure, Success}
import scala.concurrent.ExecutionContext.Implicits.global

object ApplicationMain extends App {
  val system = ActorSystem("MyActorSystem")

  val pattern = "*.txt"
  val pathPattern1 = PathPattern("c:\\tmp",pattern)
  val pathPattern3 = PathPattern("c:\\tmp2",pattern)
  val pathPattern2 = PathPattern("/home/avi/Downloads",pattern)
   implicit val timeout = Timeout(10 seconds)

  val monitorActor = system.actorOf(FileMonitoringAggregatorActor(List(pathPattern1,pathPattern2)))
  monitorActor ! Init(true)

  monitorActor ! AddPath(pathPattern3)
//
//  Thread.sleep(1000)
//  monitorActor ! RemovePath("c:\\tmp")

//
//  Thread.sleep(10000)
//  val changes = ask(monitorActor , GetNextBulks).mapTo[List[ByteBulks]]
//
//  changes.onComplete{
//    case Success(b) => b.foreach(println)
//  }

  system.awaitTermination()
}