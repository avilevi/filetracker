package org.avi.monitor

object CommonMessages {

  case class Init(includeExistingFiles: Boolean)
  case object Stop
  case object Stopped
}
