package org.avi.monitor

import java.io.IOException
import java.nio.ByteBuffer
import java.nio.channels.{AsynchronousFileChannel, CompletionHandler}
import java.nio.file.Path
import java.nio.file.StandardOpenOption._

import scala.concurrent.{ExecutionContext, Future, Promise}
import scala.util.Try

/**
  * Reads file in async way
  * this part is basically copied from this nice implementation https://gist.github.com/tovbinm/f73849aff169d1ebeb97
  */
object FileAsyncIO {
  /**
    *
    * @param file - Path the file to read from
    * @param position - Long starting position
    * @param ec - ExecutionContext for handling futures
    * @return
    */
  def read(file: Path, position: Long = 0L)(implicit ec: ExecutionContext): Future[Array[Byte]] = {
    val p = Promise[Array[Byte]]()
    try {
      val channel = AsynchronousFileChannel.open(file, READ)
      val buffer = ByteBuffer.allocate(channel.size().toInt)
      channel.read(buffer, position, buffer, onComplete(channel, p))
    }
    catch {
      case t: Throwable => p.failure(t)
    }
    p.future
  }

  private def onComplete(channel: AsynchronousFileChannel, p: Promise[Array[Byte]]) = {
    new CompletionHandler[Integer, ByteBuffer]() {
      def completed(res: Integer, buffer: ByteBuffer) {
        p.complete(Try {
          buffer.array().take(res)
        })
        closeSafely(channel)
      }

      def failed(t: Throwable, buffer: ByteBuffer) {
        p.failure(t)
        closeSafely(channel)
      }
    }
  }

  private def closeSafely(channel: AsynchronousFileChannel) =
    try {
      channel.close()
    } catch {
      case e: IOException =>
    }

}