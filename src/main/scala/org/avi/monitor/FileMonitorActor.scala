package org.avi.monitor

import java.nio.file.StandardWatchEventKinds._
import java.nio.file.{Files, Path, PathMatcher}

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import org.avi.monitor.CommonMessages._

import scala.collection.JavaConversions._
import scala.collection.mutable
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}


case object GetBulk
case class ByteBulks(data:Map[String, Array[Byte]])

/**
  * companion object
  */
object FileMonitorActor {
  /**
    *
    * @param directory - Path to be monitored
    * @param matcher - PathMatcher (globe pattern ) only files that matches will be monitored
    * @return
    */
  def apply(directory: Path, matcher: PathMatcher) = Props(new FileMonitorActor(directory, matcher))
}

/**
  * Monitors all files in a directory that matches a globe based pattern
  * for examples checkout this post https://blogs.oracle.com/thejavatutorials/entry/pathmatcher_in_nio_2
  * this actor creates a watchService actor and register to it in order to be notified upon any change .
  * when getting an Init message it sends a watch request to the watch actor
  * it keeps a state of filePos that holds for each monitored file the last position
  * and a queue the keeps changes in a queue and dequeue it upon request to get the changes
  * @param dir - the directory that will be monitored by this actor
  * @param matcher - PathMatcher , a globe based pattern that will be used to filter only the desired files
  */
class FileMonitorActor(dir: Path, matcher: PathMatcher) extends Actor with ActorLogging {

  val filePos = new mutable.HashMap[Path, Long]
  val fileQueue = new mutable.HashMap[Path, mutable.Queue[Array[Byte]]]
  var watchActor: Option[ActorRef] = None

  override def receive: Receive = ready

  def ready: Receive = {
    case Init(includeExisting) =>
      watchActor = Some(context.actorOf(WatchServiceActor(self)))
      if (includeExisting)
        monitorExistingFiles()
      context.become(inService)
      watchActor foreach (_ ! Watch(dir))
    case msg =>
      log.error(s"File Monitor Got Unknown Message $msg")
  }

  def inService: Receive = {
    case EventOccured(event, path) if matcher.matches(path.getFileName) =>
      log.info(s" Received $event at path $path ")
      event match {
        case ENTRY_CREATE => addFileToQueue(path)
        case ENTRY_MODIFY => process(path)
        case ENTRY_DELETE => removeFileFromQueue(path)
      }
    case GetBulk =>
      val bulks = getNextBulk
      bulks foreach (sender ! _)
    case Stop =>
      log.info(s"Stopping monitor actor for $dir")
      context.stop(self)
    case _ => /*do nothing*/
  }

  def removeFileFromQueue(p: Path): Unit = {
    filePos -= p
    fileQueue -= p
    log.debug(s"$p removed ")
  }

  /**
    * reads file from last known position.
    * and adds the the byte array to the queue
    * @param file - the file to process
    */
  def process(file: Path) {
    filePos.get(file) match {
      case Some(position) =>
        FileAsyncIO.read(file, position).onComplete { b =>
          val str = b match {
            case Success(bs) =>
              if (bs.nonEmpty && bs.length > 0)
                updateQueue(file, bs)
            case Failure(e) =>
              log.error(s"Failed to read ${file.toAbsolutePath} at position $position Error: ${e.getMessage}")
          }
        }
      case None => //todo check recovery mode
    }
  }

  def updateQueue(file: Path, bs: Array[Byte]) = {
    filePos.update(file, filePos(file) + bs.length)
    fileQueue(file.toAbsolutePath) += bs
    log.debug(s"Queue for $file updated with: ${new String(bs)} ")
  }

  def getNextBulk: Option[ByteBulks] = {
    val data = fileQueue.collect { case (p, q) if q.nonEmpty =>
      val bulk = fileQueue(p).dequeue()
      p.toAbsolutePath.toString -> bulk
    }.toMap
    if (data.nonEmpty) {
      log.info(s"Return Bulks for ${data.map(x => s"${x._1} -> ${new String(x._2)}")}")
      Some(ByteBulks(data))
    }
    else
      None
  }

  def monitorExistingFiles() = Files.newDirectoryStream(dir).filter(f =>
    matcher.matches(f.getFileName) && !filePos.containsKey(f)).foreach(addFileToQueue)

  def addFileToQueue(p: Path): Unit = {
    filePos += (p -> 0)
    fileQueue += (p.toAbsolutePath -> new mutable.Queue[Array[Byte]])
    log.debug(s"$p is now monitored ")
  }

  def filePosState = filePos

  def fileQueueState = fileQueue
}
