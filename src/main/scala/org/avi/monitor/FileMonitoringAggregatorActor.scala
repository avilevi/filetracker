package org.avi.monitor

import java.nio.file._

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import org.avi.monitor.CommonMessages._

import scala.collection.mutable
import scala.concurrent.duration._

case object GetNextBulks
case object RequestNextBulk
case class AddPath(path:PathPattern,includeExisting:Boolean = true)
case class RemovePath(path:String)
case class PathPattern(path:String,pattern:String)


object FileMonitoringAggregatorActor {
  def apply(paths:List[PathPattern]) = Props(new FileMonitoringAggregatorActor(paths))
}

class FileMonitoringAggregatorActor(paths:List[PathPattern]) extends Actor with ActorLogging {
  import MonitorImplicits._

  import scala.concurrent.ExecutionContext.Implicits.global

  val bulksBuffer = new mutable.ArrayBuffer[ByteBulks]
  val monitorActors = paths.collect {
    case p:PathPattern if Files.exists(p.path) =>
      p.path -> context.actorOf(FileMonitorActor(p.path, p.pattern), p.path.removeSlashes)
  }(collection.breakOut):mutable.HashMap[String, ActorRef]


  override def receive: Receive = ready

  def ready: Receive = {
    case i:Init =>
      log.info("initialized")
      monitorActors.values foreach (_ ! i)
      context become monitoring
      self ! RequestNextBulk
  }

  def monitoring: Receive = {

    case GetNextBulks =>
      log.info("Sending back "+bulksBuffer)
      sender ! bulksBuffer.toList
      bulksBuffer.clear()
    case RequestNextBulk =>
      monitorActors.values foreach (_ ! GetBulk)
      context.system.scheduler.scheduleOnce(500 millis, self, RequestNextBulk)
    case bs: ByteBulks =>
      bulksBuffer += bs
    case Stop =>
      monitorActors.values foreach (_ ! Stop)
      context become ready
    case AddPath(p,i)=>
      if (monitorActors.contains(p.path))
        log.warning(s"Request Add ${p.path} is redundant because it is already monitored ")
      else{
        if (Files.exists(p.path)) {
          val m = context.actorOf(FileMonitorActor(p.path, p.pattern), p.path.removeSlashes)
          monitorActors += p.path -> m
          m ! Init(i)
        }else
          log.error(s"Cannot add path. Reason: Directory ${p.path} does not exists")
      }
    case RemovePath(p) =>
      monitorActors.find(_._1 == p) match {
        case Some(a) =>
          log.info(s"Remove ${a._1} from monitor")
          a._2 ! Stop
          monitorActors -= a._1
        case None => log.warning(s"Cannot remove $p. Reason: Not Found ")
      }
  }
}
