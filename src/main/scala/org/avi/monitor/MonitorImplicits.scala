package org.avi.monitor

import java.nio.file.{FileSystems, Path, PathMatcher, Paths}

/**
  * Created by avi on 19/11/2015.
  */
object MonitorImplicits {
  implicit def strToPath(path:String):Path = Paths get path
  implicit def patternToMatcher(pattern:String):PathMatcher = FileSystems.getDefault.getPathMatcher(s"glob:" + pattern)

  implicit class StringManipulations(s: String) {
    def removeSlashes = s.replaceAll("[/\\\\]", "_")
  }
}
