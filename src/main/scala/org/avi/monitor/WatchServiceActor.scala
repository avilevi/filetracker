package org.avi.monitor

import java.nio.file.StandardWatchEventKinds._
import java.nio.file.{FileSystems, Path, WatchEvent, WatchKey}

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import org.avi.monitor.CommonMessages.Stopped

import scala.collection.JavaConversions._
import scala.concurrent.duration._

case object Poll

case object Interrupt

case class EventOccured(event: WatchEvent.Kind[_], path: Path)

case class Watch(path: Path)

object WatchServiceActor {
  def apply(notifyActor: ActorRef,monitoredTypes:Array[WatchEvent.Kind[_]] =  Array(ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY)
  ) = Props(new WatchServiceActor(notifyActor,monitoredTypes))
}


class WatchServiceActor(notifyActor: ActorRef,monitoredTypes:Array[WatchEvent.Kind[_]]) extends Actor with ActorLogging {

  import scala.concurrent.ExecutionContext.Implicits.global

  private val watchService = FileSystems.getDefault.newWatchService()

  def receive: Receive = {
    case Watch(path) =>
      val fileAtPath = path.toFile
      if (fileAtPath.isDirectory)
        path.register(watchService, monitoredTypes)
      else
        path.getParent.register(watchService, monitoredTypes)
      context.become(monitor)
      self ! Poll
    case msg =>
      println("Got unhandled message" + msg)

  }

  def monitor: Receive = {
    case Poll =>
      val key = watchService.take()
      key.pollEvents() foreach { event =>
        val relativePath = event.context.asInstanceOf[Path]
        val path = contextAbsolutePath(key, relativePath)
        event.kind match {
          case kind: WatchEvent.Kind[_] if monitoredTypes.contains(kind) =>
            notifyActor ! EventOccured(kind, path)
          case _ => // do nothing
        }
      }
      key.reset()
      context.system.scheduler.scheduleOnce(500 millis, self, Poll)
    case Interrupt =>
      log.info(s"Stopping watch actor ")
      context.stop(self)
  }

  /**
   * Returns the absolute path for a given event.context().asInstancOf[Path]
   * path taken from a WatchService event
   *
   * This is actually taken from http://www.javacodegeeks.com/2013/04/watchservice-combined-with-akka-actors.html
   * The way it works is a context is taken from the events originating
   * from a WatchKey, which itself is registered to an actual watchable
   * object. In this case, the watchable object is a Path. By using
   * the watchable object as a path and calling resolve, passing in the relative
   * path obtained from the context, we obtain the "full" relative-to-watchable path.
   * Afterwards, toAbsolutePath is called to get the real absolute path.
   *
   * @param key WatchKey
   * @param contextPath Path relative to the key's watchable
   * @return Path
   */
  def contextAbsolutePath(key: WatchKey, contextPath: Path): Path = {
    key.watchable().asInstanceOf[Path].resolve(contextPath).toAbsolutePath
  }
}
