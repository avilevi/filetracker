package org.avi.monitor

import java.io.{BufferedWriter, FileWriter}
import java.nio.file.StandardWatchEventKinds._
import java.nio.file.{FileSystems, Files, Path, Paths}

import akka.actor.ActorSystem
import akka.testkit.{TestActorRef, TestKit, TestProbe}
import org.scalatest._

class FileMonitorActorSpec extends TestKit(ActorSystem("testSystem"))
with FlatSpecLike
with ShouldMatchers
with BeforeAndAfter {

  trait TestBase {
    val tempDirPath = Files.createTempDirectory("tester")
    val tempFileInTempDir = Files.createTempFile(tempDirPath.toAbsolutePath, "hello", ".log")
    tempDirPath.toFile.deleteOnExit()
    tempFileInTempDir.toFile.deleteOnExit()
    val pattern: String = "*.log"
    val matcher = FileSystems.getDefault.getPathMatcher(s"glob:" + pattern)
    val monitorActorRef = TestActorRef(new FileMonitorActor(tempDirPath, matcher))
    val monitorActor = monitorActorRef.underlyingActor
    val defaultMsg = "This is a test message"
    val modifyEvent = EventOccured(ENTRY_MODIFY, tempFileInTempDir)
    val createEvent = EventOccured(ENTRY_CREATE, tempFileInTempDir)

    def msgWriter(msg: String = defaultMsg) = {
      val writer = new BufferedWriter(new FileWriter(tempFileInTempDir.toFile,true))
      writer.write(msg)
      writer.close()
    }
  }

  "WatchServiceActor event handling " should "add file to queue when new file CREATED in monitored directory" in {
    new TestBase {
      monitorActor.context.become(monitorActor.inService)
      monitorActorRef ! createEvent
      assert(monitorActor.fileQueueState.exists(p => p._1 == tempFileInTempDir))
      monitorActor.filePosState.get(tempFileInTempDir) shouldEqual Some(0L)
    }
  }
  it should "set positions to queue when file changed in monitored directory" in {
    new TestBase {
      monitorActor.context.become(monitorActor.inService)
      msgWriter()
      monitorActorRef ! createEvent
      monitorActorRef ! modifyEvent
      Thread.sleep(1000)
      monitorActor.filePosState.get(tempFileInTempDir) shouldEqual Some(defaultMsg.length)
    }

  }
  it should "set positions to queue when file with several changes in monitored directory" in {
    new TestBase {
      monitorActor.context.become(monitorActor.inService)
      msgWriter()
      Thread.sleep(1000)
      monitorActorRef ! createEvent
      monitorActorRef ! modifyEvent
      Thread.sleep(1000)
      msgWriter()
      monitorActorRef ! modifyEvent
      Thread.sleep(1000)
      monitorActor.filePosState.get(tempFileInTempDir) shouldEqual Some(defaultMsg.length * 2)
    }
  }
  it should "return bulk with changes" in {
    new TestBase {
      monitorActor.context.become(monitorActor.inService)
      msgWriter()
      monitorActorRef ! createEvent
      monitorActorRef ! modifyEvent
      Thread.sleep(1000)
      monitorActorRef tell(GetBulk, testActor)
      expectMsgPF() {
        case ByteBulks(bulk) =>
//          val data = bulk.head
          val dt = bulk.get(tempFileInTempDir.toString)
          new String(dt.head) shouldEqual defaultMsg
      }
    }
  }
  it should "return ordered changes" in {
    new TestBase {
      monitorActor.context.become(monitorActor.inService)
      monitorActorRef ! createEvent
      val messages = List("first\n", "second\n")
      messages foreach { m =>
        msgWriter(m)
        monitorActorRef ! modifyEvent
      }
      Thread.sleep(1000)
      messages foreach { m =>
        monitorActorRef tell(GetBulk, testActor)
        expectMsgPF() {
          case ByteBulks(bulk) =>
//            val data = bulk.head
            val dt = bulk.get(tempFileInTempDir.toString)
            dt match {
              case Some(d) => new String(d) shouldEqual m
              case None => assert(false)
            }
        }
      }
    }
  }
}