package org.avi.monitor

import java.io.{File, BufferedWriter, FileWriter}
import java.nio.file.{Path, Files}

import akka.actor.ActorSystem
import akka.testkit.{TestProbe, TestActorRef, TestKit}
import org.avi.monitor.CommonMessages.Init
import org.scalatest._


class FileMonitoringAggregationActorSpec extends TestKit(ActorSystem("testSystem"))
with FlatSpecLike
with ShouldMatchers
with StopSystemAfterAll {

  trait TestBase {
    val tempDirPath = Files.createTempDirectory("tester")
    val tempDirPath2 = Files.createTempDirectory("tester2")
    val tempFileInTempDir = Files.createTempFile(tempDirPath.toAbsolutePath, "hello", ".log")
    val tempFileInTempDir2 = Files.createTempFile(tempDirPath2.toAbsolutePath, "hello2", ".log")
    tempDirPath.toFile.deleteOnExit()
    tempDirPath2.toFile.deleteOnExit()
    tempFileInTempDir.toFile.deleteOnExit()
    tempFileInTempDir2.toFile.deleteOnExit()
    val pattern = "*.log"
    val pathPattern1 = PathPattern(tempDirPath.toAbsolutePath.toString, pattern)
    val pathPattern2 = PathPattern(tempDirPath2.toAbsolutePath.toString, pattern)
    val defaultMsg = "This is a test message"
    def msgWriter(file:File,msg: String = defaultMsg) = {
      val writer = new BufferedWriter(new FileWriter(file,true))
      writer.write(msg)
      writer.close()
    }
  }

  "FileMonitoringAggregationActor" should "capture changes from single directory " in {
    new TestBase {
      val monitorActorRef = TestActorRef(new FileMonitoringAggregatorActor(List(pathPattern1, pathPattern2)))
      val probe = TestProbe()
      monitorActorRef ! Init(true)
      Thread.sleep(1000)
      msgWriter(tempFileInTempDir.toFile)
      Thread.sleep(1000)
      monitorActorRef tell(GetNextBulks, probe.ref)
      probe.expectMsgPF() {
        case bs: List[ByteBulks@unchecked] if bs.isInstanceOf[List[ByteBulks]] =>
            val d = for {
              b <- bs
              bulk <-b.data
            }yield (bulk._1,new String(bulk._2))
            val expected = List((tempFileInTempDir.toAbsolutePath.toString, defaultMsg))
            assert (d == expected)

          case unExpected =>
            println("Got " + unExpected)
            assert(false)
        }
    }
  }
  it should "capture ordered changes from several directories " in {
      new TestBase {
        val writer = new BufferedWriter(new FileWriter(tempFileInTempDir.toFile))
        val probe = TestProbe()
        // Actor
        val monitorActorRef = TestActorRef(new FileMonitoringAggregatorActor(List(pathPattern1, pathPattern2)))
        monitorActorRef ! Init(true)
        Thread.sleep(1000)
       msgWriter(tempFileInTempDir.toFile)
        Thread.sleep(2000)
        val msg = "Hello world"
        msgWriter(tempFileInTempDir2.toFile,msg)
        Thread.sleep(2000)

        monitorActorRef tell(GetNextBulks, probe.ref)

        probe.expectMsgPF() {
          case bs: List[ByteBulks@unchecked] if bs.isInstanceOf[List[ByteBulks]] =>
            val d = for {
            b <- bs
            bulk <-b.data
            }yield (bulk._1,new String(bulk._2))
            val expected = List(
              (tempFileInTempDir.toAbsolutePath.toString, defaultMsg),
              (tempFileInTempDir2.toAbsolutePath.toString,msg)
            )
            assert (d == expected)

          case unExpected =>
            println("Got " + unExpected)
            assert(false)
        }
      }
    }
  it should "remove Path " in {
    new TestBase {
      val writer = new BufferedWriter(new FileWriter(tempFileInTempDir.toFile))
      val probe = TestProbe()
      // Actor
      val monitorActorRef = TestActorRef(new FileMonitoringAggregatorActor(List(pathPattern1, pathPattern2)))
      monitorActorRef ! Init(true)
      Thread.sleep(1000)
      msgWriter(tempFileInTempDir.toFile)
      monitorActorRef ! RemovePath(pathPattern2.path)
      Thread.sleep(1000)
      val msg = "Hello world"
      msgWriter(tempFileInTempDir2.toFile, msg)
      Thread.sleep(1000)

      monitorActorRef tell(GetNextBulks, probe.ref)

      probe.expectMsgPF() {
        case bs: List[ByteBulks@unchecked] if bs.isInstanceOf[List[ByteBulks]] =>
          val d = for {
            b <- bs
            bulk <- b.data
          } yield (bulk._1, new String(bulk._2))
          val expected = List(
            (tempFileInTempDir.toAbsolutePath.toString, defaultMsg)
          )
          assert(d == expected)

        case unExpected =>
          println("Got " + unExpected)
          assert(false)
      }
    }
  }
  it should "Add Path " in {
    new TestBase {
      val writer = new BufferedWriter(new FileWriter(tempFileInTempDir.toFile))
      val probe = TestProbe()
      // Actor
      val monitorActorRef = TestActorRef(new FileMonitoringAggregatorActor(List(pathPattern1)))
      monitorActorRef ! Init(true)
      Thread.sleep(1000)
      msgWriter(tempFileInTempDir.toFile)
      val msg = "Hello world"
      monitorActorRef ! AddPath(pathPattern2)
      Thread.sleep(1000)
      msgWriter(tempFileInTempDir2.toFile, msg)
      Thread.sleep(1000)

      monitorActorRef tell(GetNextBulks, probe.ref)

      probe.expectMsgPF() {
        case bs: List[ByteBulks@unchecked] if bs.isInstanceOf[List[ByteBulks]] =>
          val d = for {
            b <- bs
            bulk <- b.data
          } yield (bulk._1, new String(bulk._2))
          val expected = List(
            (tempFileInTempDir.toAbsolutePath.toString, defaultMsg),
            (tempFileInTempDir2.toAbsolutePath.toString, msg)
          )
          assert(d == expected)

        case unExpected =>
          println("Got " + unExpected)
          assert(false)
      }
    }
  }
}
