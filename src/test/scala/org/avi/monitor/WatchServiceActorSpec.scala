package org.avi.monitor

import java.io.{BufferedWriter, FileWriter}
import java.nio.file.Files
import java.nio.file.StandardWatchEventKinds._

import akka.actor.ActorSystem
import akka.testkit.TestKit
import org.scalatest._

class WatchServiceActorSpec extends TestKit(ActorSystem("testSystem"))
with FlatSpecLike
with ShouldMatchers
with BeforeAndAfter {
  val tempDirPath = Files.createTempDirectory("root")
  val tempFileInTempDir = Files.createTempFile(tempDirPath.toAbsolutePath, "hello", ".log")
  tempDirPath.toFile.deleteOnExit()
  tempFileInTempDir.toFile.deleteOnExit()

  "WatchServiceActor" should "notify when file CREATED in monitored directory" in {

    val watchActor = system.actorOf(WatchServiceActor(testActor))
    watchActor ! Watch(tempDirPath)
    Thread.sleep(1000)
    Files.createTempFile(tempDirPath, "new_file", ".log").toFile.deleteOnExit()
    expectMsgPF() {
      case EventOccured(e, _) => e shouldEqual ENTRY_CREATE
      case msg =>
        println("FAIL got " + msg)
        assert(false)
    }
  }
  it should "notify when file CHANGED in monitored directory" in {

    val watchActor = system.actorOf(WatchServiceActor(testActor))
    watchActor ! Watch(tempDirPath)
    Thread.sleep(1000)
    val writer = new BufferedWriter(new FileWriter(tempFileInTempDir.toFile))
    val msg = "There's text in here wee!!\n"
    writer.write(msg)
    writer.close
    Thread.sleep(1000)
    expectMsgPF() {
      case EventOccured(e, _) => e shouldEqual ENTRY_MODIFY
      case otherEntry =>
        println("FAIL got " + otherEntry)
        assert(false)
    }
  }
  it should "notify when file DELETED in monitored directory" in {

    val watchActor = system.actorOf(WatchServiceActor(testActor))
    val f = Files.createTempFile(tempDirPath, "new_file", ".log").toFile
    watchActor ! Watch(tempDirPath)
    Thread.sleep(1000)
    f.delete()

    expectMsgPF() {
      case EventOccured(e, _) => e shouldEqual ENTRY_MODIFY
      case msg =>
        println("FAIL got " + msg)
        assert(false)
    }
  }

}

